#include <Servo.h>          
#include <NewPing.h>        

#define in1 11  // Motor1 L298 Pin in1
#define in2 10  // Motor1 L298 Pin in2
#define in3 9   // Motor2 L298 Pin in3
#define in4 6   // Motor2 L298 Pin in4


#define trig_pin 4 
#define echo_pin 5 
#define maximum_distance 200

int distance = 100;

NewPing sonar(trig_pin, echo_pin, maximum_distance); 
Servo servo_motor; 

void setup(){
  pinMode(in1, OUTPUT);
  pinMode(in2, OUTPUT);
  pinMode(in3, OUTPUT);
  pinMode(in4, OUTPUT);

  servo_motor.attach(A5); 
  servo_motor.write(115);
  delay(2000);
  distance = readPing();
  delay(100);
}
void loop(){
  int distanceRight = 0;
  int distanceLeft = 0;
  delay(50);
  if (distance <= 25){
    moveStop();
    delay(300);
    distanceRight = lookRight();
    delay(300);
    distanceLeft = lookLeft();
    delay(300);
    if (distanceRight >= distanceLeft){
      turnRight();
      delay(390);
      moveForward();
      delay(450);
      turnLeft();
      delay(550);
      moveForward();
      delay(750);
      moveStop();
      delay(200);
      turnLeft();
      delay(400);
      moveStop();
      delay(200);
      moveForward();
      delay(640);
      turnRight();
      delay(430);
      moveStop();
    }
    else if (distanceLeft > distanceRight){
      turnLeft();
      delay(410);
      moveForward();
      delay(600);
      moveStop();
      delay(200);
      turnRight();
      delay(400);
      moveForward();
      delay(780);
      moveStop();
      delay(200);
      turnRight();
      delay(370);
      moveStop();
      delay(200);
      moveForward();
      delay(500);
      turnLeft();
      delay(350);
    }
  }
  else{
    moveForward();
  }
    distance = readPing();
}
int lookRight(){

  servo_motor.write(50);
  delay(500);
  int distance = readPing();
  delay(100);
  servo_motor.write(115);
  return distance;
}
int lookLeft(){

  servo_motor.write(170);
  delay(500);
  int distance = readPing();
  delay(100);
  servo_motor.write(115);
  return distance;
  delay(100);
}
int readPing(){
  delay(70);
  int cm = sonar.ping_cm();
  if (cm==0){
    cm=250;
  }
  return cm;
}
void moveStop(){
  digitalWrite(in1, LOW);
  digitalWrite(in2, LOW);
  digitalWrite(in3, LOW);
  digitalWrite(in4, LOW);
}
void moveForward(){

    digitalWrite(in1, LOW);   // Stânga Motor înapoi
    digitalWrite(in2, HIGH);  // Stânga Motor înainte
    digitalWrite(in3, HIGH);  // Dreapta Motor înainte
    digitalWrite(in4, LOW);
  
}
void moveBackward(){

  digitalWrite(in1, HIGH);  // Stânga Motor înapoi
  digitalWrite(in2, LOW);   // Stânga Motor înainte
  digitalWrite(in3, LOW);   // Dreapta Motor înainte
  digitalWrite(in4, HIGH);
}
void turnRight(){

  digitalWrite(in1, HIGH);  // Stânga Motor înapoi
  digitalWrite(in2, LOW);   // Stânga Motor înainte
  digitalWrite(in3, HIGH);  // Dreapta Motor înainte
  digitalWrite(in4, LOW);
  
}
void turnLeft(){

    digitalWrite(in1, LOW); // Motorul A înainte
    digitalWrite(in2, HIGH); 
    digitalWrite(in3, LOW); // Motorul B înapoi
    digitalWrite(in4, HIGH);
    
}