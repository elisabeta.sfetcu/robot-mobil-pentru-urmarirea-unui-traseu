#define MANUAL_MODE 1
#define AUTONOMOUS_MODE 0


#define in1 11  // Motor1 L298 Pin in1
#define in2 10  // Motor1 L298 Pin in2
#define in3 9   // Motor2 L298 Pin in3
#define in4 6   // Motor2 L298 Pin in4

#define L_S A0  
#define R_S A1  

#include <SoftwareSerial.h>

int mode = -1;  
SoftwareSerial bluetooth(12, 13);
void setup() {
  Serial.begin(9600);  

  pinMode(R_S, INPUT);
  pinMode(L_S, INPUT);

  pinMode(in1, OUTPUT);
  pinMode(in2, OUTPUT);
  pinMode(in3, OUTPUT);
  pinMode(in4, OUTPUT);

  stop();
  delay(500);
  bluetooth.begin(9600);
}
void loop() {
  uint8_t comm;
  //while(bluetooth.available()<=0){}
  if (bluetooth.available() > 0) {
    comm = bluetooth.read();
    Serial.println((char)comm);
    switch (comm) {
      case 'A':
        mode = AUTONOMOUS_MODE;
        break;
      case 'M':
        mode = MANUAL_MODE;
        stop();
        break;
      case 'F':
        if (mode == MANUAL_MODE)
          forward();
        break;
      case 'S':
        if (mode == MANUAL_MODE)
          stop();
        break;
      case 'B':
        if (mode == MANUAL_MODE)
          backward();
        break;
      case 'R':
        if (mode == MANUAL_MODE)
          turnRight();
        break;
      case 'L':
        if (mode == MANUAL_MODE)
          turnLeft();
        break;
    }
  }
  if (mode == AUTONOMOUS_MODE) {
    int rightSensor = digitalRead(R_S);
    int leftSensor = digitalRead(L_S);
    if ((!rightSensor) && (leftSensor)) {
      turnLeft();
      delay(100);
      bluetooth.write("left\n");
    }
    if ((!leftSensor) && (rightSensor)) {
      turnRight();
      delay(100);
      bluetooth.write("right\n");
    }
    if (!rightSensor && !leftSensor) {
      forward(100);
      bluetooth.write("forward\n");
    }
    if (rightSensor && leftSensor) {
      stop();
      bluetooth.write("stop\n");
    }
    delayMicroseconds(10);    
  }

}


void forward(uint8_t speed) {
  analogWrite(in1, LOW);// Stânga Motor înapoi
  analogWrite(in2, speed);// Stânga Motor înainte
  analogWrite(in3, speed);// Dreapta Motor înainte
  analogWrite(in4, LOW);// Dreapta Motor înapoi
}
void forward() {
  digitalWrite(in1, LOW);   
  digitalWrite(in2, HIGH);  
  digitalWrite(in3, HIGH);  
  digitalWrite(in4, LOW);   
}
void backward() {
  digitalWrite(in1, HIGH);  
  digitalWrite(in2, LOW);   
  digitalWrite(in3, LOW);   
  digitalWrite(in4, HIGH);  
}
void turnLeft() {
  digitalWrite(in1, HIGH);  
  digitalWrite(in2, LOW);   
  digitalWrite(in3, HIGH);  
  digitalWrite(in4, LOW);   
}
void turnRight() {
  digitalWrite(in1, LOW);   
  digitalWrite(in2, HIGH);  
  digitalWrite(in3, LOW);   
  digitalWrite(in4, HIGH);  
}
void backward(uint8_t speed) {
  analogWrite(in1, speed);// Stânga Motor înapoi
  analogWrite(in2, LOW);// Stânga Motor înainte
  analogWrite(in3, LOW);// Dreapta Motor înainte
  analogWrite(in4, speed);// Dreapta Motor înapoi
}

void turnLeft(uint8_t speed) {
  analogWrite(in1, speed);// Stânga Motor înapoi
  analogWrite(in2, LOW);// Stânga Motor înainte
  analogWrite(in3, speed);// Dreapta Motor înainte
  analogWrite(in4, LOW);// Dreapta Motor înapoi
}


void turnRight(uint8_t speed) {
  analogWrite(in1, LOW);// Stânga Motor înapoi
  analogWrite(in2, speed);// Stânga Motor înainte
  analogWrite(in3, LOW);// Dreapta Motor înainte
  analogWrite(in4, speed);// Dreapta Motor înapoi
}

void stop() {
  digitalWrite(in1, LOW);  // Stânga Motor înapoi
  digitalWrite(in2, LOW);  // Stânga Motor înainte
  digitalWrite(in3, LOW);  // Dreapta Motor înainte
  digitalWrite(in4, LOW);  // Dreapta Motor înapoi
}
