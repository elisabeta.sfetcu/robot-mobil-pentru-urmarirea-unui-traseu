#include <SoftwareSerial.h>
#include <Servo.h>          
#include <NewPing.h>

#define MANUAL_MODE 1
#define AUTONOMOUS_MODE 0

#define in1 11  // Motor1 L298 Pin in1
#define in2 10  // Motor1 L298 Pin in2
#define in3 9   // Motor2 L298 Pin in3
#define in4 6   // Motor2 L298 Pin in4

#define L_S A0  
#define R_S A1  

#define trig_pin 4 
#define echo_pin 5 
#define maximum_distance 200

int distance = 100;
NewPing sonar(trig_pin, echo_pin, maximum_distance); 
Servo servo_motor;

int mode = -1;  
SoftwareSerial bluetooth(12, 13);
void setup() {
  Serial.begin(9600);  

  pinMode(R_S, INPUT);
  pinMode(L_S, INPUT);

  pinMode(in1, OUTPUT);
  pinMode(in2, OUTPUT);
  pinMode(in3, OUTPUT);
  pinMode(in4, OUTPUT);

  servo_motor.attach(A5); 
  servo_motor.write(115);
  delay(2000);
  distance = readPing();
  delay(100);

  stop();
  delay(500);
  bluetooth.begin(9600);
}
void loop() {
  uint8_t comm;
  //while(bluetooth.available()<=0){}
  if (bluetooth.available() > 0) {
    comm = bluetooth.read();
    Serial.println((char)comm);
    switch (comm) {
      case 'A':
        mode = AUTONOMOUS_MODE;
        break;
      case 'M':
        mode = MANUAL_MODE;
        stop();
        break;
      case 'F':
        if (mode == MANUAL_MODE)
          forward();
        break;
      case 'S':
        if (mode == MANUAL_MODE)
          stop();
        break;
      case 'B':
        if (mode == MANUAL_MODE)
          backward();
        break;
      case 'R':
        if (mode == MANUAL_MODE)
          turnRight();
        break;
      case 'L':
        if (mode == MANUAL_MODE)
          turnLeft();
        break;
    }
  }
  if (mode == AUTONOMOUS_MODE) {
    int rightSensor = digitalRead(R_S);
    int leftSensor = digitalRead(L_S);
    int distanceRight = 0;
    int distanceLeft = 0;
    distance = readPing();
    delay(50);
    if (distance <= 25){
    stop();
    delay(300);
    backward();
    delay(200);
    stop();
    delay(300);
    distanceRight = lookRight();
    delay(300);
    distanceLeft = lookLeft();
    delay(300);
    if (distanceRight >= distanceLeft){
      turnLeft();
      delay(410);
      forward();
      delay(600);
      turnRight();
      delay(500);
      forward();
      delay(600);
      stop();
      delay(200);
      turnRight();
      delay(500);
      stop();
      delay(200);
      forward();
      delay(600);
      turnLeft();
      delay(410);
    }
    else if (distanceLeft > distanceRight){
      turnRight();
      delay(410);
      forward();
      delay(600);
      stop();
      delay(200);
      turnLeft();
      delay(410);
      forward();
      delay(600);
      stop();
      delay(200);
      turnLeft();
      delay(410);
      stop();
      delay(200);
      forward();
      delay(600);
      turnRight();
      delay(410);
    }
  }
    else if ((!rightSensor) && (leftSensor)) {
      turnLeft();
      delay(100);
      bluetooth.write("left\n");
    }
    else if ((!leftSensor) && (rightSensor)) {
      turnRight();
      delay(100);
      bluetooth.write("right\n");
    }
    else if (!rightSensor && !leftSensor) {
      forward(150);
      bluetooth.write("forward\n");
    }
    else if (rightSensor && leftSensor) {
      stop();
      bluetooth.write("stop\n");
    }
    delayMicroseconds(10);    
  }

}

int lookRight(){

  servo_motor.write(50);
  delay(500);
  int distance = readPing();
  delay(100);
  servo_motor.write(115);
  return distance;
}
int lookLeft(){

  servo_motor.write(170);
  delay(500);
  int distance = readPing();
  delay(100);
  servo_motor.write(115);
  return distance;
  delay(100);
}
int readPing(){
  delay(70);
  int cm = sonar.ping_cm();
  if (cm==0){
    cm=250;
  }
  return cm;
}

void forward(uint8_t speed) {
  analogWrite(in1, LOW);// Stânga Motor înapoi
  analogWrite(in2, speed);// Stânga Motor înainte
  analogWrite(in3, speed);// Dreapta Motor înainte
  analogWrite(in4, LOW);// Dreapta Motor înapoi
}
void forward() {
  digitalWrite(in1, LOW);   
  digitalWrite(in2, HIGH);  
  digitalWrite(in3, HIGH);  
  digitalWrite(in4, LOW);   
}
void backward() {
  digitalWrite(in1, HIGH);  
  digitalWrite(in2, LOW);   
  digitalWrite(in3, LOW);   
  digitalWrite(in4, HIGH);  
}
void turnLeft() {
  digitalWrite(in1, HIGH);  
  digitalWrite(in2, LOW);   
  digitalWrite(in3, HIGH);  
  digitalWrite(in4, LOW);   
}
void turnRight() {
  digitalWrite(in1, LOW);   
  digitalWrite(in2, HIGH);  
  digitalWrite(in3, LOW);   
  digitalWrite(in4, HIGH);  
}
void backward(uint8_t speed) {
  analogWrite(in1, speed);// Stânga Motor înapoi
  analogWrite(in2, LOW);// Stânga Motor înainte
  analogWrite(in3, LOW);// Dreapta Motor înainte
  analogWrite(in4, speed);// Dreapta Motor înapoi
}

void turnLeft(uint8_t speed) {
  analogWrite(in1, speed);// Stânga Motor înapoi
  analogWrite(in2, LOW);// Stânga Motor înainte
  analogWrite(in3, speed);// Dreapta Motor înainte
  analogWrite(in4, LOW);// Dreapta Motor înapoi
}


void turnRight(uint8_t speed) {
  analogWrite(in1, LOW);// Stânga Motor înapoi
  analogWrite(in2, speed);// Stânga Motor înainte
  analogWrite(in3, LOW);// Dreapta Motor înainte
  analogWrite(in4, speed);// Dreapta Motor înapoi
}

void stop() {
  digitalWrite(in1, LOW);  // Stânga Motor înapoi
  digitalWrite(in2, LOW);  // Stânga Motor înainte
  digitalWrite(in3, LOW);  // Dreapta Motor înainte
  digitalWrite(in4, LOW);  // Dreapta Motor înapoi
}
